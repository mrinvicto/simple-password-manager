import {StyleSheet} from 'react-native';
const getStyleSheet = (theme) => {
  return StyleSheet.create({
    homeContainer: {
      display: 'flex',
      height: '100%',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    passwordDetailsContainer: {},
  });
};

export default getStyleSheet;
