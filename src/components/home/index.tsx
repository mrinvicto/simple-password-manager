import React from 'react';
import {View} from 'react-native';
import {TextInput} from 'react-native-paper';
import {withTheme} from 'react-native-paper';
import getStyleSheet from './styles';

const HomeComponent = ({theme, ...props}) => {
  const classes = getStyleSheet(theme);

  return (
    <View style={classes.homeContainer}>
      <TextInput />
    </View>
  );
};

export default withTheme(HomeComponent);
