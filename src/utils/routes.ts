import {IRoute, RouteAccessLevel} from '../models/app/IRoute';
import HomeScreen from '../screens/home';

const routesInfo: IRoute[] = [
  {
    id: 1,
    path: '/',
    name: 'Home',
    accessLevel: RouteAccessLevel.PUBLIC,
    component: HomeScreen,
  },
];

export default routesInfo;
