import React from 'react';
import {SafeAreaView} from 'react-native';
import HomeContainer from '../containers/home';

const HomeScreen = () => {
  return (
    <SafeAreaView>
      <HomeContainer />
    </SafeAreaView>
  );
};

export default HomeScreen;
