import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {IRoute, RouteAccessLevel} from '../../models/app/IRoute';
import appRoutes from '../../utils/routes';

const Stack = createStackNavigator();

const Router = (props) => {
  // Gets all public routes from app
  const allPubliRoutes = (): IRoute[] =>
    appRoutes.filter(
      (routeDetails) => routeDetails.accessLevel === RouteAccessLevel.PUBLIC,
    );

  // Gets all valid routes based on the current app state
  const getAllValidAppRoutes = (): IRoute[] => {
    return allPubliRoutes();
  };

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {getAllValidAppRoutes().map((routeDetails: IRoute) => {
        return (
          <Stack.Screen
            key={`${routeDetails.id}_${routeDetails.name}`}
            name={routeDetails.name}
            component={routeDetails.component}
          />
        );
      })}
    </Stack.Navigator>
  );
};

export default Router;
