export interface IRoute {
  id: number;
  name: string;
  component: () => JSX.Element;
  path: string;
  accessLevel: RouteAccessLevel;
}

export enum RouteAccessLevel {
  PUBLIC, // Routes which are accessible for all type of users authenticated/non-authenticated
  AUTHENTICATED, // Routes which are accessible to only logged-in users
  NON_AUTHENTICATED, // Routes which are accessible to only non logged-in users
}
